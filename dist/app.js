'use strict';

require('isomorphic-fetch');

// fetch('http://www.pallier.org/extra/liste.de.mots.francais.frgut.txt')
//   .then(response => response.text())
//   .then(data => {
//     const arr = data.split("\r\n");
//     let finalArr = [];

//     for (let i = 1; i < 26; i++) {
//       const newArr = arr.filter(word => word.length === i);
//       finalArr.push(newArr);
//     }

//     console.log(finalArr);

//   })
//   .catch(error => console.error(error));

// <---- REAL TRY


var hashedWord = function hashedWord(str) {
  return str.toLowerCase().split('').sort().join('');
}; // https://www.notion.so/Anagram-0fe8ab76c6fa43b39aa0deaf35f05431


// <---- REAL TRY

fetch('http://www.pallier.org/extra/liste.de.mots.francais.frgut.txt').then(function (response) {
  return response.text();
}).then(function (data) {
  // create an object with to gather words with their hash
  var hashedWords = data.split("\r\n").reduce(function (result, word) {
    if (result[hashedWord(word)] === undefined) result[hashedWord(word)] = [];
    result[hashedWord(word)].push(word);
    return result;
  }, {});

  // get only pair with more than one word
  var anagrams = Object.values(hashedWords).filter(function (value) {
    return value.length > 1;
  });

  console.log('anagrams', anagrams);
}).catch(function (error) {
  return console.error(error);
});