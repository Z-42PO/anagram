// https://www.notion.so/Anagram-0fe8ab76c6fa43b39aa0deaf35f05431

import "isomorphic-fetch";

// <---- REAL TRY
// fetch('http://www.pallier.org/extra/liste.de.mots.francais.frgut.txt')
//   .then(response => response.text())
//   .then(data => {
//     const arr = data.split("\r\n");
//     let finalArr = [];

//     for (let i = 1; i < 26; i++) {
//       const newArr = arr.filter(word => word.length === i);
//       finalArr.push(newArr);
//     }
    
//     console.log(finalArr);
    
//   })
//   .catch(error => console.error(error));
// <---- REAL TRY


const hashedWord = str => {
  return str.toLowerCase().split('').sort().join('');
}

fetch('http://www.pallier.org/extra/liste.de.mots.francais.frgut.txt')
  .then(response => response.text())
  .then(data => {
    // create an object to gather words by hash
    const hashedWords = data.split("\r\n").reduce((result, word) => {
      if (result[hashedWord(word)] === undefined) result[hashedWord(word)] = [];
      result[hashedWord(word)].push(word);
      return result;
    }, {});

    // get only group with more than one word
    const anagrams = Object.values(hashedWords).filter(value => value.length > 1);

    console.log('anagrams', anagrams);
  })
  .catch(error => console.error(error));
